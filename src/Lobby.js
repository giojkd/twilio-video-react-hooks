import React from "react";



const Lobby = ({
  username,
  handleUsernameChange,
  roomName,
  handleRoomNameChange,
  handleSubmit,
  connecting,
}) => {

  

  return (
    <form onSubmit={handleSubmit}>
      <h2>Entra nella stanza</h2>
      <div style={{ 'display': 'none' }}>
        <label htmlFor="name">Il tuo nome:</label>
        <input
          type="text"
          id="field"
          value={username}
          onChange={handleUsernameChange}
          readOnly={connecting}
          required
        />
      </div>

      <div style={{ 'display': 'none' }}>
        <label htmlFor="room">Identificativo della stanza:</label>
        <input
          type="text"
          id="room"
          value={roomName}
          onChange={handleRoomNameChange}
          readOnly={connecting}
          required
        
        />
      </div>

      <button type="submit" disabled={connecting}>
        {connecting ? "Connessione in corso..." : "Accedi"}
      </button>
    </form>
  );
};

export default Lobby;
