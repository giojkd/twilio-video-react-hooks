import React from 'react';
import './App.css';
import VideoChat from './VideoChat';
import { Router } from "react-router-dom";
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();


const App = () => {
  return (
    <div className="app">
      {/* 
      <header>
        <h1>Video Chat with Hooks</h1>
      </header>
      */}
      <Router {...{ history }}>
        <main>
          <VideoChat />
        </main>
      </Router>
      {/* 
      <footer>
        <p>
          Made with{' '}
          <span role="img" aria-label="React">
            ⚛️
          </span>{' '}
          by <a href="https://twitter.com/philnash">philnash</a>
        </p>
      </footer>
      */}
    </div>
  );
};

export default App;
